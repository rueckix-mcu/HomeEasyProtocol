/**
 * @file sniffer.cpp
 * @author rueckix
 * @brief RF-443 MHz sniffer using CC1101. Useful for decoding unknown RF remote controls
 * @version 0.1
 * @date 2021-03-06
 * 
 * @copyright Copyright (c) 2021
 * 
 * 
 *
 * 
 */

#include <Arduino.h>

#include <ELECHOUSE_CC1101_SRC_DRV.h>

#include "capture.h"
#include "decoder.h"
#include "command.h"
#include "encoder.h"

#include "debug.h"

#define GDO2_RX  2
#define GDO0_TX  3
#define SCK   13
#define CSN   10
#define SO    12
#define SI    11

/**
 * @brief TODO: kleine FB sendet zwar 11000111100 preamble aber Gruppencode 1000 und der Status ist invertiert
 * 
 */

// amount of LOW pulses to capture 

#define REPEAT 3

Capture cap = Capture(GDO2_RX);
Decoder dec = Decoder();

#ifndef CODE_LEN
#define CODE_LEN 100
#endif
CODETYPE output_code[CODE_LEN];

#define HANDLE_HE300
#define HANDLE_HE841
#define HANDLE_INTERTECHNO

// #define EMIT_INVERT // enable to send an inverted signal after receiving and decoding correctly

void setup() {
  Serial.begin(9600);


  if (ELECHOUSE_cc1101.getCC1101()){       // Check the CC1101 Spi connection.
  DBGPRINTln(F("Connection OK"));
  }else{
  DBGPRINTln(F("Connection Error"));
  }

  ELECHOUSE_cc1101.setSpiPin(SCK, SO, SI, CSN);
  

  ELECHOUSE_cc1101.Init();            // must be set to initialize the cc1101!


  ELECHOUSE_cc1101.setMHZ(433.92); // Here you can set your basic frequency. The lib calculates the frequency automatically (default = 433.92).The cc1101 can: 300-348 MHZ, 387-464MHZ and 779-928MHZ. Read More info from datasheet.
   
  ELECHOUSE_cc1101.SetRx();     // set Receive on
  
  //pinMode(GDO2_RX, INPUT);
  
  
  DBGPRINTln(F("Receiver initialized... "));
  
};
  



void loop()
{ 

  uint16_t len = cap.CaptureRaw();
  

  if (len)
  {
    DBGPRINT(F("Capture Length: "));
    DBGPRINTln(len);
//    DBGPRINT(F("Free Memory: "));
//    DBGPRINTln(freeMemory());

    //cap.PrintBuffer();
    PulseBuffer pulsebuf;
    cap.GetBuffer(pulsebuf);
    bool ret;
#ifdef HANDLE_HE300    
    ret = dec.Decode(pulsebuf, Protocol::HE300);
    

    if (ret)
    {
      DBGPRINTln(F("Decoding HE300"));
      dec.PrintBuffer();
      CodeBuffer codebuf;
      dec.GetBuffer(codebuf);
      CommandParameters c1;
      if (Command::Parse(Protocol::HE300, codebuf, c1))
      {
        Command::PrintCommand(c1);

        DBGPRINTln(F("Encoding HE300"));
        
        CodeBuffer out;
        out.length = PROTO_LIST[Protocol::HE300].codeLen;
        out.buffer = output_code;
        Command::Generate(Protocol::HE300, c1, out);
        Decoder::PrintBuffer(out.buffer, out.length);

        // invert status
        c1.status = !c1.status;

        // generate command again
        Command::Generate(Protocol::HE300, c1, out);
        Decoder::PrintBuffer(out.buffer, out.length);

        // sleep
        DBGPRINTln(F("Inverting command..."));
        delay(1000);

        DBGPRINTln(F("Emitting command..."));
        // emit
        
        Encoder enc(GDO0_TX, Protocol::HE300);
        
        #ifdef EMIT_INVERT
       
        ELECHOUSE_cc1101.Init(); 
        ELECHOUSE_cc1101.SetTx(); 
        enc.Emit(out, REPEAT);
        ELECHOUSE_cc1101.Init(); 
        ELECHOUSE_cc1101.SetRx(); 
         #endif
       

      }
      else
      {
        DBGPRINTln(F("Command invalid"));
      }
    }
#endif

#ifdef HANDLE_HE841
    ret = dec.Decode(pulsebuf, Protocol::HE841);
    

    if (ret)
    {
      DBGPRINTln(F("Decoding HE841"));
      dec.PrintBuffer();
      CodeBuffer codebuf;
      dec.GetBuffer(codebuf);
      CommandParameters c1;
      if (Command::Parse(Protocol::HE841, codebuf, c1))
      {
        Command::PrintCommand(c1);

        DBGPRINTln(F("Encoding HE841"));
        
        CodeBuffer out;
        out.length = PROTO_LIST[Protocol::HE841].codeLen;
        out.buffer = output_code;
        Command::Generate(Protocol::HE841, c1, out);
        Decoder::PrintBuffer(out.buffer, out.length);

        // invert status
        c1.status = !c1.status;

        // generate command again
        Command::Generate(Protocol::HE841, c1, out);
        Decoder::PrintBuffer(out.buffer, out.length);

        // sleep
        DBGPRINTln(F("Inverting command..."));
        delay(1000);

        DBGPRINTln(F("Emitting command..."));
        // emit
        Encoder enc(GDO0_TX, Protocol::HE841);

       #ifdef EMIT_INVERT
        ELECHOUSE_cc1101.Init(); 
        ELECHOUSE_cc1101.SetTx(); 
        enc.Emit(out, REPEAT);
        ELECHOUSE_cc1101.Init(); 
        ELECHOUSE_cc1101.SetRx(); 
      #endif
      }
      else
      {
        DBGPRINTln(F("Command invalid"));
      }
    }
#endif

#ifdef HANDLE_INTERTECHNO
    DBGPRINTln(F("*********************************************"));

    ret = dec.Decode(pulsebuf, Protocol::INTERTECHNO);
    if (ret)
    {
      DBGPRINTln(F("Decoding INTERTECHNO"));
      dec.PrintBuffer();

      CodeBuffer codebuf;
      dec.GetBuffer(codebuf);
      CommandParameters c1;
      if (Command::Parse(Protocol::INTERTECHNO, codebuf, c1))
      {
        Command::PrintCommand(c1);

        DBGPRINTln(F("Encoding INTERTECHNO"));
        
        CodeBuffer out;
        out.length = PROTO_LIST[Protocol::INTERTECHNO].codeLen;
        out.buffer = output_code;
        Command::Generate(Protocol::INTERTECHNO, c1, out);
        Decoder::PrintBuffer(out.buffer, out.length);
        

        // invert status
        c1.status = !c1.status;

        // generate command again
        Command::Generate(Protocol::INTERTECHNO, c1, out);
        Decoder::PrintBuffer(out.buffer, out.length);
        
        // sleep
        DBGPRINTln(F("Inverting command..."));
        delay(1000);

        DBGPRINTln(F("Emitting command..."));
        // emit
        Encoder enc(GDO0_TX, Protocol::INTERTECHNO);

        #ifdef EMIT_INVERT
        ELECHOUSE_cc1101.Init(); 
        ELECHOUSE_cc1101.SetTx(); 
        enc.Emit(out, REPEAT);
        ELECHOUSE_cc1101.Init(); 
        ELECHOUSE_cc1101.SetRx(); 
        #endif
  
      }
      else
      {
        DBGPRINTln(F("Command invalid"));
      }
    }
#endif
    
    DBGPRINTln(F("============================================="));
  }
}

