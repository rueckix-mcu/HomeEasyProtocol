#include "protocols.h"


/**
 * @brief Empirically determined protocol parameters
 * 
 * The format is as follows.
 * 
 * 1) Short pulse length
 * 2) Range for a long pulse as a multiplier of the short pulse length. 3 components are (min, nominal, max)
 * 3) true iff the protocol has a dedicated start pulse
 * 4) Range for the start pulse. Same semantics as in 2)
 * 5) Range for the stop pulse. Same semantics as in 2)
 * 6) Number of LOW pulses per protocol frame, EXCLUDING start and stop pulses 
 * 7) Number of bits represented by the pulse code
 */
 

CODETYPE PROTO_HE300_PREAMBLE[] = {1,1,0,0,0,1,1,1,1,0,0};
CODETYPE PROTO_HE300_NOGROUP[] = {1,0,1,1};
CODETYPE PROTO_HE300_GROUP[] = {1,1,0,0};


ProtocolParameters PROTO_HE300 = {300, {2, 4, 5}, false, {0, 0, 0}, {25, 34, 40}, 57, 57}; 
ProtocolParameters PROTO_HE841 = {270, {3, 5, 6}, true, {7, 10, 15}, {25, 32, 40}, 57, 57}; 
ProtocolParameters PROTO_INTERTECHNO = {260, {3, 5, 6}, true, {7, 10, 15}, {20, 40, 80}, 64, 32}; 

ProtocolParameters PROTO_LIST[Protocol::NONE] = {
                                    PROTO_HE300, 
                                    PROTO_HE841,
                                    PROTO_INTERTECHNO
                                  };


