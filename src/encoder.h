#pragma once


#include "protocols.h"

#ifndef OUTPUT_PULSE_LEN
#define OUTPUT_PULSE_LEN 100
#endif

class Encoder
{
protected:
    size_t _pin;
    PULSETYPE _pulses[OUTPUT_PULSE_LEN];
    uint16_t _bufferSize = OUTPUT_PULSE_LEN;
    Protocol _protocol;

public:
    Encoder(size_t pin, Protocol protocol);
    ~Encoder();

protected:
    void reset();
    void pulseHE300(const CodeBuffer &code);
    void pulseHE841(const CodeBuffer &code);
    void pulseIntertechno(const CodeBuffer &code);

public:
    void Emit(const CodeBuffer &code, size_t repeat = 1);
};

