#pragma once

#include <Arduino.h>

/**
 * @brief Encoding assumption
 * 
 * Machester coding assumes an alternating HIGH, LOW, HIGH, LOW...
 * We are only capturing the duration of the LOW. Highs are the clock signal.
 */


#define CODETYPE    uint8_t
#define PULSETYPE   uint16_t


enum Protocol
{
    HE300 = 0,
    HE841 = 1,
    INTERTECHNO = 2,
    // insert new protocols before NONE. NONE is used as a counter
    NONE = 3
};

struct Range
{
    PULSETYPE min;
    PULSETYPE norm;
    PULSETYPE max;
};


struct CodeBuffer
{
    CODETYPE* buffer;
    uint16_t length;
};

struct PulseBuffer
{
    PULSETYPE* buffer;
    uint16_t length;
};


struct ProtocolParameters
{
    PULSETYPE shortPulse;
    Range longPulse;
    bool hasStartPulse;
    Range startPulse;
    Range stopPulse;
    uint16_t frameLen; // frame length excluding start and stop, only counting LOW pulses
    uint16_t codeLen; // code length in bits when decoded
};


extern ProtocolParameters PROTO_HE300;
extern CODETYPE PROTO_HE300_PREAMBLE[];
extern CODETYPE PROTO_HE300_NOGROUP[];
extern CODETYPE PROTO_HE300_GROUP[];


extern ProtocolParameters PROTO_INTERTECHNO;

extern ProtocolParameters PROTO_LIST[NONE];