#pragma once

#include "encoder.h"
#include "command.h"

#ifndef CODE_LEN
#define CODE_LEN 100
#endif

class MultiProtocolEncoder: protected Encoder
{
private:
    CODETYPE output_code[CODE_LEN];
    uint16_t _bufferSize = CODE_LEN;
    CodeBuffer _buf = {output_code, _bufferSize};
    

public:
    MultiProtocolEncoder(size_t pin);

public:
    /**
     * @brief Emits a command on all supported protocols and encodings
     * 
     * @param param 
     * @param repeat 
     */
    void Emit(const CommandParameters &param, size_t repeat = 1);

};