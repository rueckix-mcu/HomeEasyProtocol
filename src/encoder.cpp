#include "encoder.h"
#include "debug.h"



Encoder::Encoder(size_t pin, Protocol protocol)
{
    _pin = pin;
    pinMode(_pin, OUTPUT);
    _protocol = protocol;
    reset();
}

Encoder::~Encoder()
{
 
}

void Encoder::reset()
{
    memset(_pulses, 0, _bufferSize * sizeof(*_pulses));
}

void Encoder::pulseHE300(const CodeBuffer &code)
{
    /**
     * @brief Format
     * SHORT <== 0 bit
     * LONG <== 1 bit
     */


    size_t j = 0;

    // no start pulse

    for (size_t i = 0; i < code.length; i++)
    {
        if (code.buffer[i] == 1)
        {
            _pulses[j++] = PROTO_LIST[_protocol].longPulse.norm * PROTO_LIST[_protocol].shortPulse;
        }
        else // == 0
        {
            _pulses[j++] = PROTO_LIST[_protocol].shortPulse;
        }
    }
    
    // stop pulse
    _pulses[j++] = PROTO_LIST[_protocol].stopPulse.norm *  PROTO_LIST[_protocol].shortPulse;
}

void Encoder::pulseHE841(const CodeBuffer &code)
{
    /**
     * @brief Format
     * SHORT <== 0 bit
     * LONG <== 1 bit
     */


    size_t j = 0;

    // start pulse
    _pulses[j++] = PROTO_LIST[_protocol].startPulse.norm * PROTO_LIST[_protocol].shortPulse;

    for (size_t i = 0; i < code.length; i++)
    {
        if (code.buffer[i] == 1)
        {
            _pulses[j++] = PROTO_LIST[_protocol].longPulse.norm * PROTO_LIST[_protocol].shortPulse;
        }
        else // == 0
        {
            _pulses[j++] = PROTO_LIST[_protocol].shortPulse;
        }
    }
    
    // stop pulse
    _pulses[j++] = PROTO_LIST[_protocol].stopPulse.norm *  PROTO_LIST[_protocol].shortPulse;
}



void Encoder::pulseIntertechno(const CodeBuffer &code)
{

    /**
     * @brief Format
     * SHORT-LONG <== 0 bit
     * LONG-SHORT <== 1 bit
     */

    size_t j = 0;

    // start pulse
    _pulses[j++] = PROTO_LIST[_protocol].startPulse.norm * PROTO_LIST[_protocol].shortPulse;

    for (size_t i = 0; i < code.length; i++)
    {
        if (code.buffer[i] == 1)
        {
            _pulses[j++] = PROTO_LIST[_protocol].longPulse.norm * PROTO_LIST[_protocol].shortPulse;
            _pulses[j++] = PROTO_LIST[_protocol].shortPulse;
        }
        else // == 0
        {
            _pulses[j++] = PROTO_LIST[_protocol].shortPulse;
            _pulses[j++] = PROTO_LIST[_protocol].longPulse.norm * PROTO_LIST[_protocol].shortPulse;
        }
    }
    
    // stop pulse
    _pulses[j++] = PROTO_LIST[_protocol].stopPulse.norm * PROTO_LIST[_protocol].shortPulse; 

}

void Encoder::Emit(const CodeBuffer &code, size_t repeat)
{
    reset();

    switch (_protocol)
    {
    case Protocol::HE300:
        pulseHE300(code);
        break;
    case Protocol::HE841:
        pulseHE841(code);
        break;
    case Protocol::INTERTECHNO:
        pulseIntertechno(code);
    default:
        break;
    }

    /*
    Serial.println("[");
    // actual emission
    for (size_t i = 0; i < _bufferSize; i++)
    {
        //Serial.print(PROTO_LIST[_protocol].shortPulse);
        //Serial.print(" ");
        Serial.print(_pulses[i]);
        Serial.print(" ");
    }

    Serial.println("]");
*/

    for (size_t i = 0; i < repeat; i++)
    {
    
        // actual emission
        for (size_t i = 0; i < _bufferSize; i++)
        {
            digitalWrite(_pin, HIGH);
            delayMicroseconds(PROTO_LIST[_protocol].shortPulse);
            digitalWrite(_pin, LOW);
            delayMicroseconds(_pulses[i]);
        }
    }
       
}