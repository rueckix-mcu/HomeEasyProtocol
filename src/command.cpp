#include "command.h"
#include "debug.h"

bool Command::Parse(Protocol protocol, const CodeBuffer &code, CommandParameters &param)
{

    switch (protocol)
    {
    case Protocol::HE300:
      return parseHE300(code,  param);

    case Protocol::HE841:
      return parseHE841(code,  param);

    case Protocol::INTERTECHNO:
      return parseIntertechno(code, param);

    default:
      return false;
    }   
    
}


bool Command::parseHE300(const CodeBuffer &code, CommandParameters &param)
{
  if (code.length < PROTO_LIST[Protocol::HE300].codeLen)
    return false;

  uint32_t preamble = 0;
  uint32_t address = 0;
  uint32_t unit = 0;
  bool status = false;
  bool group = false;

  
  preamble = binToDec(&(code.buffer[0]), 11);  
  if (preamble != HE300_PREAMBLE)
    return false;

  uint32_t val_groupA = binToDec(&(code.buffer[43]), 4);
  // check group flags
  if (val_groupA != 11 && val_groupA != 12)
    return false;

  address = binToDec(&(code.buffer[11]), 32); 
  status = code.buffer[47]; 
  group = code.buffer[49]; 
  unit = binToDec(&(code.buffer[51]), 6);

  param.address = address;
  param.unit = unit;
  param.status = status;
  param.group = group;

  return true;
}

bool Command::parseHE841(const CodeBuffer &code, CommandParameters &param)
{
  if (code.length < PROTO_LIST[Protocol::HE841].codeLen)
    return false;

  uint32_t preamble = 0;
  uint32_t address = 0;
  uint32_t unit = 0;
  bool status = false;
  bool group = false;

  
  preamble = binToDec(&(code.buffer[0]), 11);  
  if (preamble != HE300_PREAMBLE)
    return false;

  uint32_t val_groupA = binToDec(&(code.buffer[42]), 4);
  // check group flags
  if (val_groupA != 11 && val_groupA != 12)
    return false;

  address = binToDec(&(code.buffer[11]), 31); 
  status = code.buffer[46]; 
  group = code.buffer[48]; 
  unit = binToDec(&(code.buffer[50]), 7);

  param.address = address;
  param.unit = unit;
  param.status = status;
  param.group = group;

  return true;
}

bool Command::parseIntertechno(const CodeBuffer &code, CommandParameters &param)
{
  if (code.length < PROTO_LIST[Protocol::INTERTECHNO].codeLen)
    return false;

  uint32_t address = 0;
  uint32_t unit = 0;
  bool status = false;
  bool group = false;
  

  // address: 26 bit  
  address = binToDec(&(code.buffer[0]), 26);

  // 1 bit group indiactor
  group = code.buffer[26];

  // 1 b it status
  status = code.buffer[27]; 

  // 4 bit unit
  unit = binToDec(&(code.buffer[28]), 4);
  
  param.address = address;
  param.unit = unit;
  param.status = status;
  param.group = group;

  return true;
}



void Command::PrintCommand(const CommandParameters &param)
{
  DBGPRINT(F("Address:\t")); DBGPRINTln(param.address);
  DBGPRINT(F("Unit:\t")); DBGPRINTln(param.unit);
  DBGPRINT(F("Status:\t")); DBGPRINTln(param.status);
  DBGPRINT(F("Group:\t")); DBGPRINTln(param.group);
}
#

bool Command::Generate(Protocol protocol, const CommandParameters &param, CodeBuffer &code)
{

    memset(code.buffer, 0, code.length * sizeof(CODETYPE));

    switch (protocol)
    {
    case Protocol::HE300:
      return genHE300(param,  code);

    case Protocol::HE841:
      return genHE841(param, code);

    case Protocol::INTERTECHNO:
      return genIntertechno(param, code);

    default:
      return false;
    }   
}


bool Command::genHE300(const CommandParameters &param, CodeBuffer &code)
{
  if (code.length < PROTO_LIST[Protocol::HE300].codeLen)
    return false;

  memset(code.buffer, 0, code.length * sizeof(*code.buffer));

  // constant values
  uint32_t preamble = HE300_PREAMBLE; // fixed value

  decToBin(preamble, &(code.buffer[0]),11);

  decToBin(param.address, &(code.buffer[11]), 32);

  // fixed bits depending on group flag
  uint32_t val_groupA;
  if (param.group)
    val_groupA = 12;
  else
    val_groupA = 11;

  decToBin(val_groupA, &(code.buffer[43]), 4);

  code.buffer[47] = param.status;
  code.buffer[48] = 1- param.status;
  code.buffer[49] = param.group;
  code.buffer[50] = 1;

  decToBin(param.unit, &(code.buffer[51]), 6);
  
  return true;
}

bool Command::genHE841(const CommandParameters &param, CodeBuffer &code)
{
  if (code.length < PROTO_LIST[Protocol::HE841].codeLen)
    return false;

  memset(code.buffer, 0, code.length * sizeof(*code.buffer));

  // constant values
  uint32_t preamble = HE300_PREAMBLE; // fixed value, same as HE300

  decToBin(preamble, &(code.buffer[0]),11);

  decToBin(param.address, &(code.buffer[11]), 31);

  // fixed bits depending on group flag
  uint32_t val_groupA;
  if (param.group)
    val_groupA = 12;
  else
    val_groupA = 11;

  decToBin(val_groupA, &(code.buffer[42]), 4);

  code.buffer[46] = param.status;
  code.buffer[47] = 1- param.status;
  code.buffer[48] = param.group;
  code.buffer[49] = 1;

  decToBin(param.unit, &(code.buffer[50]), 7);
  
  return true;
}


bool Command::genIntertechno(const CommandParameters &param, CodeBuffer &code)
{
  
  if (code.length < PROTO_LIST[Protocol::INTERTECHNO].codeLen)
    return false;

  
  memset(code.buffer, 0, code.length * sizeof(*code.buffer));


  decToBin(param.address, &(code.buffer[0]), 26);


  code.buffer[26] = param.group;
 
  code.buffer[27] = param.status;
  
  // 4 bit unit
  decToBin(param.unit, &(code.buffer[28]), 4);

  return true;

}


uint32_t Command::binToDec(CODETYPE *binary, size_t len)
{
  uint8_t *idx = binary;
  uint32_t val = 0;

  for (size_t i = 0; i < len; i++)
  {
    val *= 2;
    val += *idx++;
  }

  return val;
}


void Command::decToBin(uint32_t val, CODETYPE *code_out, size_t len)
{
  uint8_t *idx = &(code_out[len-1]);

  for (size_t i = 0; i < len; i++)
  {
    *idx-- = (val & B1);
    val >>= 1;
  }

}


void Command::decTo2Bin(uint32_t val, CODETYPE *code_out, size_t len)
{
  uint8_t *idx = &(code_out[2*len-1]);

  for (size_t i = 0; i < len; i++)
  {
    switch (val & B1)
    {
      case 0:
        *idx-- = 1;
        *idx-- = 0;
      break;
      case 1:
        *idx-- = 0;
        *idx-- = 1;
      break;
    }
    val >>= 1;
  }

}
