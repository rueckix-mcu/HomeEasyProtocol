#include "decoder.h"
#include "debug.h"

Decoder::Decoder()
{
    reset();
}

Decoder::~Decoder()
{
    
}


bool Decoder::Decode(const PulseBuffer &pulses, Protocol protocol)
{
    reset();



    _pulses = pulses;
    _protocol = protocol;

    // sanity checks up front
    ProtocolParameters proto = PROTO_LIST[_protocol];

    /*
    DBGPRINTln(F("Starting Decode...")); 
    DBGPRINT(F("Protocol: "));
    DBGPRINTln(_protocol);
*/
    uint16_t short_max = proto.shortPulse * 2; // TODO: we are assuming this is good enough to differentiate short and long for relevant protocols
    uint16_t long_min = proto.shortPulse * proto.longPulse.min;
    uint16_t long_max = proto.shortPulse * proto.longPulse.max;

    if (_pulses.buffer == NULL)
    {
        //DBGPRINTln(F("No buffer"));
        return false;
    }

    if (_protocol == Protocol::NONE)
        return false;

    
    

    while (true) 
    {
        bool ok = false;
        ok = findNextPulseSequence();
        if (!ok)
            return false; // nothing left to decode

        // DBGPRINTln(F("Found candiate sequence"));

        // check if all pulses in between are within bounds
        // TODO: check also short pulses if necessary
        
        ok = true;

        // we are excluding endIndex because that is the stop pulse. 
        // we are excluding the startIndex if the protocol has one
        for (size_t i = _startPulseIndex + 1 * proto.hasStartPulse; i < _endPulseIndex; i++)
        {
            // short pulse
            if (_pulses.buffer[i] < short_max)
                continue;
            
            if (_pulses.buffer[i] < long_min || _pulses.buffer[i] > long_max)
            {
                ok = false;
                break;
            }
        }

        if (!ok)
        {
            //DBGPRINTln(F("Rejecting [wrong pulse length]"));
            continue; // skip this candidate
        }

        // now we have a syntactically valid candidate to decode
        ok = true;
        bool groupok = true;
        size_t code_len = 0;

        switch (_protocol)
        {
        case Protocol::HE300: 
            
            /**
             * @brief Format
             * SHORT ==> 0 bit
             * LONG ==> 1 bit
             */            
            
            /*
            for (size_t i = _startPulseIndex; i < _endPulseIndex; i++)
            {
                Serial.print(_pulses.buffer[i]);
                Serial.print(F(" "));    
            }
            Serial.println();
            */
            for (size_t i = _startPulseIndex; i < _endPulseIndex; i++) 
            {
                if (_pulses.buffer[i] > long_min)
                {
                    _code[code_len++] = 1;
                }
                else
                {
                    _code[code_len++] = 0;
                }
            } 


            // Verify the preamble 11000111100

            for (size_t i = 0; i < 11 ; i++)
            {
                if (_code[i] != PROTO_HE300_PREAMBLE[i])
                    ok = false;
            }

            // verify the group flags (either matches 1100 or 1011)
            ok = false;
            groupok = true;
            for (size_t i = 0; i < 4 ; i++)
            {
                if (_code[43+i] != PROTO_HE300_NOGROUP[i])
                    groupok = false;
            }
            
            ok = ok || groupok;

            groupok = true;
            for (size_t i = 0; i < 4 ; i++)
            {
                if (_code[43+i] != PROTO_HE300_GROUP[i])
                    groupok = false;
            }

            ok = ok || groupok;
            

            break;

        case Protocol::HE841:  // essentially the same as HE300 but with a shorter address (1 bit less) and longer unit (1 bit more)
            
            /**
             * @brief Format
             * SHORT ==> 0 bit
             * LONG ==> 1 bit
             */            
            
            /*
            for (size_t i = _startPulseIndex; i < _endPulseIndex; i++)
            {
                Serial.print(_pulses.buffer[i]);
                Serial.print(F(" "));    
            }
            Serial.println();
            */

            for (size_t i = _startPulseIndex; i < _endPulseIndex; i++) 
            {
                if (_pulses.buffer[i] > long_min)
                {
                    _code[code_len++] = 1;
                }
                else
                {
                    _code[code_len++] = 0;
                }
            } 


            // Verify the preamble 11000111100

            for (size_t i = 0; i < 11 ; i++)
            {
                if (_code[i] != PROTO_HE300_PREAMBLE[i])
                    ok = false;
            }

            // verify the group flags (either matches 1100 or 1011)
            ok = false;
            groupok = true;
            for (size_t i = 0; i < 4 ; i++)
            {
                if (_code[42+i] != PROTO_HE300_NOGROUP[i])
                    groupok = false;
            }
            
            ok = ok || groupok;

            groupok = true;
            for (size_t i = 0; i < 4 ; i++)
            {
                if (_code[42+i] != PROTO_HE300_GROUP[i])
                    groupok = false;
            }

            ok = ok || groupok;
            

            break; 
        
        case Protocol::INTERTECHNO:
            /**
             * @brief Format
             * SHORT-LONG ==> 0 bit
             * LONG-SHORT ==> 1 bit
             */

            for (size_t i = _startPulseIndex ; i < _endPulseIndex; i +=2) 
            {
                if (_pulses.buffer[i] > long_min && _pulses.buffer[i+1] < short_max)
                {
                    _code[code_len++] = 1;
                }
                else if (_pulses.buffer[i] < short_max && _pulses.buffer[i+1] > long_min)
                {
                    _code[code_len++] = 0;
                }
                else
                {
                    //DBGPRINTln(F("Rejecting [wrong 01/10 pulse sequence]"));
                    ok = false;
                    break;
                }
            } 

            break;

        case Protocol::NONE:
            break;

        default:
            break;
        }

        if (ok)
        {
            _codeLen = code_len;
            return true;
        }
        else
        {
            continue; // skip this candidate
        }

    }

    

}


bool Decoder::findNextPulseSequence()
{
    // sanity checks up front
    ProtocolParameters proto = PROTO_LIST[_protocol];
    uint16_t start_min = proto.shortPulse * proto.startPulse.min;
    uint16_t start_max = proto.shortPulse * proto.startPulse.max;
    
    uint16_t stop_min = proto.shortPulse * proto.stopPulse.min;
    uint16_t stop_max = proto.shortPulse * proto.stopPulse.max;

/*
    DBGPRINTln(F("Starting findNextPulseSequence"));
    DBGPRINTln(F("startmin\tstartmax\tendmin\tendmax"));
    
    DBGPRINT(start_min);

    DBGPRINT(F("\t"));
    DBGPRINT(start_max);

    DBGPRINT(F("\t"));
    DBGPRINT(stop_min);

    DBGPRINT(F("\t"));
    DBGPRINTln(stop_max);
*/

    if (_pulses.buffer == NULL)
        return false;

    if (_protocol == Protocol::NONE)
        return false;

    while (true) 
    {
        if (_startPulseIndex >= _pulses.length -1)
        {
            //DBGPRINTln(F("\tStartPulseIndex too large"));
            return false;
        }

        if (_endPulseIndex >= _pulses.length -1)
        {
            //DBGPRINTln(F("\tEndPulseIndex too large"));
            return false;
        }

        bool ok = false;

        // find index of next end pulse as per protocol specification.
        for (size_t i = _endPulseIndex + 1; i < _pulses.length; i++)
        {
            if (_pulses.buffer[i] > stop_min && _pulses.buffer[i] < stop_max)
            { 
                _endPulseIndex = i;
                ok = true;
                break; // stop searching
            }
        }

        if (!ok)
        {
            //DBGPRINTln(F("\tNo further Stop pulses"));
            return false; // no more end markers found
        }

        //DBGPRINT(F("\tFound Stop pulse: "));
        //DBGPRINT(_endPulseIndex);
        //DBGPRINT(F(":\t"));
        //DBGPRINTln(_pulses.buffer[_endPulseIndex]);


        // backtrack from the end marker (if possible)
        if (proto.frameLen > _endPulseIndex)
        {
            //DBGPRINTln(F("\tNot enough pulses left"));
            continue;
        }

        // candidate start index
        _startPulseIndex = _endPulseIndex - proto.frameLen;

        

        if (!proto.hasStartPulse)
        {
            //DBGPRINTln(F("\tFound Start [no marker]"));
            return true;
        }
        

        // check for valid start pulse
        if (_pulses.buffer[_startPulseIndex  - 1* proto.hasStartPulse] > start_min && _pulses.buffer[_startPulseIndex] < start_max)
        {
            //DBGPRINTln(F("\tFound Start [marker]"));
            return true;
        }

        //DBGPRINTln(F("\tFinding next..."));

    }
}

void Decoder::reset()
{
    memset(_code, 0, _bufferSize * sizeof(*_code));
    _codeLen = 0;

    _startPulseIndex = 0;
    _endPulseIndex = 0;
    _pulses.buffer = NULL;
    _pulses.length = 0;
    _protocol = Protocol::NONE;
}


void Decoder::PrintBuffer(CODETYPE *buf, size_t len)
{
    DBGPRINT(F("["));
    for (size_t i = 0; i < len; i++)
    {
        DBGPRINT(buf[i]);
    }
    
    DBGPRINTln(F("]"));
}
void Decoder::PrintBuffer()
{
    PrintBuffer(_code, _codeLen);
}

void Decoder::GetBuffer(CodeBuffer &buf)
{
    buf.buffer = _code;
    buf.length = _codeLen;


}


