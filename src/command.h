#pragma once

#include <Arduino.h>
#include "protocols.h"


struct CommandParameters
{
    uint32_t address;
    uint32_t unit;
    bool group;
    bool status;
};


#define HE300_PREAMBLE 1596


class Command
{

private:

        /**
         * @brief Convert decimal input value to its binary representation
         * 
         * @param val         - input value
         * @param code_out    - pointer to output array
         * @param len         - number of binary digits to write. The least significant bit will be written to the last position (LittleEndian)
         */
        static void decToBin(uint32_t val, CODETYPE *code_out, size_t len);

        /**
         * @brief Convert decimal input value to a special binary representation (0-> 01, 1->10)
         * 
         * @param val         - input value
         * @param code_out    - pointer to output array
         * @param len         - number of binary digits to write. The least significant bit will be written to the last position (LittleEndian)
         */
        static void decTo2Bin(uint32_t val, CODETYPE *code_out, size_t len);


        /**
         * @brief Convert binary sequence to decimal
         * 
         * @param binary    - poiter to binary array
         * @param len       - number of bits to convert
         * @return uint32_t - decimal output. The first bit in the input array is the most significant bit (LittleEndian)
         */
        static uint32_t binToDec(CODETYPE *binary, size_t len);

        
        static bool parseHE300(const CodeBuffer &code, CommandParameters &param);
        static bool parseHE841(const CodeBuffer &code, CommandParameters &param);
        static bool parseIntertechno(const CodeBuffer &code, CommandParameters &param);

        static bool genHE300(const CommandParameters &param, CodeBuffer &code);
        static bool genHE841(const CommandParameters &param, CodeBuffer &code);
        static bool genIntertechno(const CommandParameters &param, CodeBuffer &code);


public:
    static bool Parse(Protocol protocol, const CodeBuffer &code, CommandParameters &param);
    static bool Generate(Protocol protocol, const CommandParameters &param, CodeBuffer &code);
    static void PrintCommand(const CommandParameters &param);

};





