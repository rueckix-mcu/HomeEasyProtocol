#pragma once

#include <Arduino.h>
#include "protocols.h"

#ifndef CAPTURE_LEN
#define CAPTURE_LEN 600
#endif

#define PULSEIN_TIMEOUT_MICROS 100000 // 100 ms timeout

class Capture
{
private:
    uint16_t _captureLen;
    uint16_t _bufferSize = CAPTURE_LEN;
    size_t _pin;

    PULSETYPE _pulses[CAPTURE_LEN];
    
public:
    Capture(size_t inputPin);
    ~Capture();

private: 
    void reset();

public:
    uint16_t CaptureRaw();
    void GetBuffer(PulseBuffer &buf);
    void PrintBuffer();
    static void PrintBuffer(PULSETYPE *buf, size_t len);
};

