#include "capture.h"
#include "debug.h"


Capture::Capture(size_t inputPin)
{
    _pin = inputPin;
    reset();
}

Capture::~Capture()
{
    
}


void Capture::PrintBuffer(PULSETYPE *buf, size_t len)
{
    DBGPRINT(F("[ "));
    for (size_t i = 0; i < len; i++)
    {
        DBGPRINT(buf[i]);
        DBGPRINT(F(" "));
    }
    
    DBGPRINTln(F("]"));
}

void Capture::PrintBuffer()
{
    PrintBuffer(_pulses, _captureLen);
}

void Capture::reset()
{
    memset(_pulses, 0, _bufferSize * sizeof(*_pulses));
    _captureLen = 0;
}

void Capture::GetBuffer(PulseBuffer &buf)
{
    
    buf.buffer = _pulses;
    buf.length = _captureLen;

}

uint16_t Capture::CaptureRaw()
{
    reset();

    pinMode(_pin, INPUT);
    // start collecting until timeout of full buffer
    PULSETYPE t = 0;
    
    // skip 200, for testing
    /*
    for (size_t i = 0; i < 200; i++)
    {
        t = pulseIn(_pin, LOW, PULSEIN_TIMEOUT_MICROS);
    }
    */

    for (size_t i = 0; i < _bufferSize; i++)
    {
        t = pulseIn(_pin, LOW, PULSEIN_TIMEOUT_MICROS);
        if (t == 0) // timed out
            break;
        else
        {
            _pulses[i] = t;
            _captureLen++;
        }
    }

    return _captureLen;
    
}
