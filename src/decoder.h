#pragma once

#include "protocols.h"

#ifndef CODE_LEN
#define CODE_LEN 100
#endif

class Decoder
{
    private: 
        Protocol _protocol;
        uint16_t _startPulseIndex;
        uint16_t _endPulseIndex;

        CODETYPE _code[CODE_LEN];
        uint16_t _codeLen;
        uint16_t _bufferSize = CODE_LEN;
        PulseBuffer _pulses;
    

    public:
        Decoder();
        ~Decoder();

    private:
        
        bool findNextPulseSequence();
        



        void reset();

    public:        
        bool Decode(const PulseBuffer &pulses, Protocol protocol);
        void GetBuffer(CodeBuffer &buf);
        void PrintBuffer();
        static void PrintBuffer(CODETYPE *buf, size_t len);


};
