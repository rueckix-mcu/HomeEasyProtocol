
#include "MultiProtocolEncoder.h"

MultiProtocolEncoder::MultiProtocolEncoder(size_t pin) 
:Encoder(pin, Protocol::NONE)
{

}

void MultiProtocolEncoder::Emit(const CommandParameters &param, size_t repeat) 
{
    Command::Generate(Protocol::HE300, param, _buf);
    _protocol = Protocol::HE300;
    Encoder::Emit(_buf, repeat);

    Command::Generate(Protocol::HE841, param, _buf);
    _protocol = Protocol::HE841;
    Encoder::Emit(_buf, repeat);

    Command::Generate(Protocol::INTERTECHNO, param, _buf);
    _protocol = Protocol::INTERTECHNO;
    Encoder::Emit(_buf, repeat);

    _protocol = Protocol::NONE;
}
